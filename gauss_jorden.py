import math
import sys
import traceback


class GaussJorden:
    def __init__(self, matrix):
        self.matrix = matrix
        self.n = len(self.matrix)

    def exec(self):
        try:
            self.__display_equations()
            
            for k in range(self.n):
                mx, s = 0, k
                for j in range(k, self.n):
                    if math.fabs(self.matrix[j][k]) <= mx:
                        continue
                    mx = math.fabs(self.matrix[j][k])
                    s = j
                if mx == 0:
                    print("Can't solve")
                    sys.exit(0)
                for j in range(self.n + 1):
                    swap = self.matrix[k][j]
                    self.matrix[k][j] = self.matrix[s][j]
                    self.matrix[s][j] = swap
                p = self.matrix[k][k]
                for j in range(k, self.n + 1):
                    self.matrix[k][j] /= p
                for i in range(self.n):
                    if i == k:
                        continue
                    d = self.matrix[i][k]
                    for j in range(k, self.n + 1):
                        self.matrix[i][j] -= d * self.matrix[k][j]
            self.__display_answers()
        except Exception as e:
            raise

    
    def __display_equations(self):
        try:
            for i in range(self.n):
                for j in range(self.n):
                    print("{:+d}x{:d} ".format(self.matrix[i][j], j + 1), end="")
                print("= {:+d}".format(self.matrix[i][self.n]))
        except Exception as e:
            raise


    def __display_answers(self):
        try:
            for k in range(self.n):
                print("x{:d} = {:f}".format(k+1, self.matrix[k][self.n]))
        except Exception as e:
            raise


try:
    matrix = [[1, 1, 5, 3, 1],
              [1, 5, 3, 1, -9],
              [3, 1, 3, 5, 1],
              [5, 3, 0, 7, 0]
             ]
    gj = GaussJorden(matrix)
    gj.exec()
except Exception as e:
    traceback.print_exc()
    sys.exit(1)

